<?php


namespace app\data;


use app\models\User;
use yii\data\BaseDataProvider;
use yii\db\ActiveQuery;

/**
 * @TODO по хорошему нужно сделать
 *      ThreeDataProvider + RecursiveColumnInterface (или как-то так)
 *
 * Class UsersThreeDataProvider
 * @package app\data
 */
class UsersThreeDataProvider extends BaseDataProvider
{
    /**
     * Prepares the data models that will be made available in the current page.
     * @return array the available data models
     */
    protected function prepareModels()
    {
        $users = $this->getQuery()
                      ->each(100);

        $result = [];
        /** @var User $user */
        foreach ($users as $user) {
            $user->referralsThree = User::getReferralsArray($user);
            $result[]             = $user;
        }

        return $result;
    }

    /**
     * Prepares the keys associated with the currently available data models.
     * @param array $models the available data models
     * @return array the keys
     */
    protected function prepareKeys($models)
    {
        return ['id', 'partner_id', 'client_uid', 'referrals'];
    }

    /**
     * Returns a value indicating the total number of data models in this data provider.
     * @return int total number of data models in this data provider.
     */
    protected function prepareTotalCount()
    {
        return $this->getQuery()->count();
    }

    /**
     * @return ActiveQuery
     */
    private function getQuery(): ActiveQuery
    {

        /**
         * @TODO в Mysql 8+ можно делать WITH RECURSIVE запросы
         *       и вместо многих запросов делать один
         *       Примерно так:
         * @code
         *      WITH RECURSIVE three (id, parent_id, client_uid) AS (
         *          select u.id, u.partner_id, u.client_uid
         *          from users u
         *          where partner_id = 0
         *          union all
         *          select us.id, us.partner_id, us.client_uid
         *          from users us
         *          inner join users us on us.partner_id = us.client_uid
         *      )
         *      select *
         *      from three
         *      limit 2;
         * @endcode
         */
        return User
            ::find()
            ->select(['id', 'partner_id', 'client_uid'])
            ->where(['partner_id' => 0])
            ->limit($this->pagination->limit)
            ->with('referrals')
            ->offset($this->pagination->offset);
    }
}