<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class User
 * @property $client_uid
 * @property $email
 * @property $gender
 * @property $fullname
 * @property $country
 * @property $region
 * @property $city
 * @property $address
 * @property $partner_id
 * @property $reg_date
 * @property $status
 * @property-read User[] $referrals
 * @property-read Account[] $accounts
 *
 * @package app\models
 */
class User extends ActiveRecord
{
    public $referralsThree = [];

    public static function tableName()
    {
        return '{{users}}';
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Relation to Users table
     *
     * @return ActiveQuery
     */
    public function getReferrals(): ActiveQuery
    {
        return $this->hasMany(self::class, ['partner_id' => 'client_uid']);
    }

    /**
     * Relation to Users table
     *
     * @return ActiveQuery
     */
    public function getAccounts(): ActiveQuery
    {
        return $this->hasMany(Account::class, ['partner_id' => 'client_uid']);
    }

    public function fields()
    {
        return array_merge(parent::fields(), ['referrals' => 'referralsThree']);
    }

    /**
     * Recursively fetch referrals users from DB
     *
     * @param User $user
     * @return array
     */
    public static function getReferralsArray(User $user): array
    {
        $result = [];

        $referrals = $user->getReferrals()
                          ->select(['id', 'partner_id', 'client_uid'])
                          ->with('referrals')
                          ->each(100);

        /** @var User $referral */
        foreach ($referrals as $referral) {
            if ($referral->getReferrals()->count() > 0) {
                $referral->referralsThree = self::getReferralsArray($referral);
            }

            $result[] = $referral;
        }

        return $result;
    }

    /**
     *  Рекурсивно проходит всх рефералов и передаёт в callback ф-ию
     *
     * @param callable $callback
     * @param User[]|null $referrals
     */
    public function recursiveWalkReferrals(callable $callback, ?array $referrals = null)
    {
        if (empty($this->referralsThree) && empty($referrals)) {
            return;
        }

        if ($referrals === null) {
            $referrals = $this->referralsThree;
        }

        foreach ($referrals as $referral) {
            $callback($referral);
            $this->recursiveWalkReferrals($callback, $referral->referralsThree);
        }
    }
}
