<?php

namespace app\models;

use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\mssql\QueryBuilder;
use yii\db\Query;

/**
 * This is the model class for table "trades".
 *
 * @property int $id
 * @property int|null $ticket
 * @property int|null $login
 * @property string|null $symbol
 * @property int|null $cmd
 * @property float|null $volume
 * @property string|null $open_time
 * @property string|null $close_time
 * @property float|null $profit
 * @property float|null $coeff_h
 * @property float|null $coeff_cr
 */
class Trade extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket', 'login', 'cmd'], 'integer'],
            [['volume', 'profit', 'coeff_h', 'coeff_cr'], 'number'],
            [['open_time', 'close_time'], 'safe'],
            [['symbol'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'ticket'     => 'Ticket',
            'login'      => 'Login',
            'symbol'     => 'Symbol',
            'cmd'        => 'Cmd',
            'volume'     => 'Volume',
            'open_time'  => 'Open Time',
            'close_time' => 'Close Time',
            'profit'     => 'Profit',
            'coeff_h'    => 'Coeff H',
            'coeff_cr'   => 'Coeff Cr',
        ];
    }

    /**
     * Calculate volume for all trades of passed users
     *
     * @param array $usersIds
     * @param \DateTime $endTime
     * @param \DateTime $startTime
     * @return float
     */
    public static function calculateSummaryVolume(array $usersIds, \DateTime $endTime, \DateTime $startTime): float
    {
        $subQuery = Trade
            ::find()
            ->alias('t')
            ->select('(t.volume * t.coeff_h * t.coeff_cr) as summary')
            ->innerJoin('accounts ac', 'ac.login = t.login')
            ->innerJoin('users us', 'us.client_uid = ac.client_uid')
            ->where(['us.id' => $usersIds])
            ->andWhere(
                ['<=', 't.close_time', $endTime->format('Y-m-d H:i:s')]
            )
            ->andWhere(
                ['>=', 't.close_time', $startTime->format('Y-m-d H:i:s')]
            )
            ->orderBy(['close_time' => SORT_ASC]);

        $sum = (new Query())
            ->from(['res' => $subQuery])
            ->sum('res.summary');

        return (float)($sum ?? 0);
    }

    /**
     * Calculate profit for all trades of passed users
     *
     * @param array $usersIds
     * @param \DateTime $endTime
     * @param \DateTime $startTime
     * @return float
     */
    public static function calculateSummaryProfit(array $usersIds, \DateTime $endTime, \DateTime $startTime): float
    {
        $subQuery = Trade
            ::find()
            ->alias('t')
            ->select('t.profit')
            ->innerJoin('accounts ac', 'ac.login = t.login')
            ->innerJoin('users us', 'us.client_uid = ac.client_uid')
            ->where(
                ['<=', 't.close_time', $endTime->format('Y-m-d H:i:s')]
            )
            ->andWhere(
                ['>=', 't.close_time', $startTime->format('Y-m-d H:i:s')]
            )
            ->andWhere(['us.id' => $usersIds])
            ->orderBy(['close_time' => SORT_ASC]);

        $sum = (new Query())
            ->from(['res' => $subQuery])
            ->sum('res.profit');

        return (float)($sum ?? 0);
    }
}
