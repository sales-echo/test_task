<?php


namespace app\controllers;


use app\data\UsersThreeDataProvider;
use app\models\Trade;
use app\models\User;
use yii\base\InvalidArgumentException;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class TestTaskController extends Controller
{
    public function actionIndex()
    {
        $provider = new UsersThreeDataProvider(
            [
                'pagination' => [
                    'pageSize' => $this->request->get('limit', 2),
                    'page'     => $this->request->get('offset', 0)
                ]
            ]
        );

        $this->response->data   = $provider->getModels();
        $this->response->format = 'json';

        return $this->response;
    }

    /**
     * @param int $id
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): Response
    {
        $user = User::findOne(['client_uid' => $id]);

        if (!$user) {
            throw new NotFoundHttpException();
        }

        switch ($this->request->get('type', 'summary')) {
            case 'volume':
                $user->referralsThree = User::getReferralsArray($user);
                $result               = $this->calculateVolume($user);
                break;
            case 'profit':
                $user->referralsThree = User::getReferralsArray($user);
                $result               = $this->calculateProfit($user);
                break;
            case 'referrals':
                $user->referralsThree = User::getReferralsArray($user);
                $result               = $this->calculateReferralsCount($user);
                break;
            default:
                throw new InvalidArgumentException('Unknown type');
        }


        $this->response->data   = [
            'user'   => $user->toArray(['id', 'fullname']),
            'result' => $result
        ];
        $this->response->format = 'json';

        return $this->response;
    }

    private function calculateVolume(User $user): float
    {
        $maxDate = new \DateTime($this->request->get('start_date', 'now'));
        $minDate = $this->request->get('end_date')
            ? new \DateTime($this->request->get('end_date'))
            : (clone $maxDate)->sub(new \DateInterval('P1D'));// -1 day

        $usersArr = [$user->getId()];

        $user->recursiveWalkReferrals(function (User $referral) use (&$usersArr) {
            $usersArr[] = $referral->getId();
        });

        return Trade::calculateSummaryVolume($usersArr, $maxDate, $minDate);
    }

    private function calculateProfit(User $user): float
    {
        $maxDate = new \DateTime($this->request->get('start_date', 'now'));
        $minDate = $this->request->get('end_date')
            ? new \DateTime($this->request->get('end_date'))
            : (clone $maxDate)->sub(new \DateInterval('P1D'));// -1 day

        $usersArr = [$user->getId()];

        $user->recursiveWalkReferrals(function (User $referral) use (&$usersArr) {
            $usersArr[] = $referral->getId();
        });

        return Trade::calculateSummaryProfit($usersArr, $maxDate, $minDate);
    }

    private function calculateReferralsCount(User $user): array
    {
        $result = [
            'firstLevel' => 0,
            'all'        => 0,
            'levels'     => 0,
        ];
        $levels = [
            [
                $user->client_uid
            ]
        ];
        $user->recursiveWalkReferrals(function (User $referral) use ($user, &$result, &$levels) {
            if ($referral->partner_id === $user->client_uid) {
                $result['firstLevel']++;
            }

            $result['all']++;

            // проверяем каждый уровень на наличие родителя
            foreach ($levels as $level => $ids) {
                // если родитель найден добавляем к уровню родителя +1 текущего клиента
                if (in_array($referral->partner_id, $ids)) {
                    $key = $level + 1;
                    // если уровня нет – создаем новый уровень
                    if (!array_key_exists($key, $levels)) {
                        $levels[$key] = [];
                    }

                    $levels[$key][] = $referral->client_uid;
                    break;
                }
            }
        });

        $result['levels'] = count($levels) - 1;

        return $result;
    }
}