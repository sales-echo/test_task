<?php

use yii\db\Migration;

/**
 * Class m200720_170511_add_trades_close_time_index
 */
class m200720_170511_add_trades_close_time_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('trades_close_time_index', 'trades', 'close_time asc');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('trades_close_time_index', 'trades');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_170511_add_trades_close_time_index cannot be reverted.\n";

        return false;
    }
    */
}
