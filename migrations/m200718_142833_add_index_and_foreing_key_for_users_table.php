<?php

use yii\db\Migration;

/**
 * Class m200718_142833_add_index_and_foreing_key_for_users_table
 */
class m200718_142833_add_index_and_foreing_key_for_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        @TODO не работает :(
//              скорее всего из-за partner_id=0, тк это
//              нарушает логику внешних ключей
//
//        $this->addForeignKey(
//            'fk-users-partner_id',
//            'users',
//            'partner_id',
//            'users',
//            'client_uid'
//            );

        $this->createIndex(
            'users_partner_id_index',
            'users',
            'partner_id',
            false
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users-partner_id','users');
        $this->dropIndex('idx-users-partner_id', 'users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_142833_add_index_and_foreing_key_for_users_table cannot be reverted.\n";

        return false;
    }
    */
}
